import logging
from telegram.ext import Updater, CallbackContext
from settings import TOKEN, DELAY, DOMAINS, CHAT_ID
import urllib.request

bot = Updater(token=TOKEN, use_context=True)
dp = bot.dispatcher
jq = bot.job_queue

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


def check_services(domains):
    failure_requests = []
    for domain in domains:
        response = urllib.request.urlopen(domain).getcode()
        if response != 200:
            failure_requests.append(domain)
    return failure_requests


def callback(context: CallbackContext):
    report = check_services(DOMAINS)
    if report:
        context.bot.send_message(chat_id=CHAT_ID,
                                 text=f'servers crashed {report}')


callback = jq.run_repeating(callback, interval=DELAY, first=10)

if __name__ == '__main__':
    bot.start_polling()
    bot.idle()
